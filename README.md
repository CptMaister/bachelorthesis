# bachelorthesis

Public repository of my bachelor thesis, submitted in December 2021 at the University of Augsburg.

All pictures can be used and modified freely. They can be found in the img folder and are SVG files created in Inkscape and embedded into the tex files as exported PDF files.

The tex-styling is based on slightly modified files of [Milan Zerbin](https://gitlab.com/MilanZerbin)'s [thesis](https://gitlab.com/MilanZerbin/bachelorthesis).

The repo that was used to work on the thesis is private as it includes not publicly available annotated literature.

Check out the website [complex-fibers.org](http://complex-fibers.org/)!

This thesis was reworked into two papers, published on [arxiv.org](https://arxiv.org/):
- [Introduction to Arnold's J+-Invariant](https://arxiv.org/abs/2210.00871)
- [J+-like Invariants under Bifurcations](https://arxiv.org/abs/2210.02968)
